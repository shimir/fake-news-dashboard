var express = require('express');
var router = express.Router();
var twitterAPI = require('../services/twitter-api');
var bot_test = require('../services/bot_test');
var DeepAI_API = require('../services/DeepAI-api');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

router.get('/get-fake-profiles/:term', async function (req, res, next) {

    let twitterProfiles = await twitterAPI.searchUsers(req.params['term']);

    if (twitterProfiles) {
    }

    res.json({title: 'fake profiles', count: twitterProfiles.length, profiles: twitterProfiles})
});

/* GET last tags on user on twitter */
/* term - number of tags wanted by user */
router.get('/get-last-tags/:tag/:number', async function (req, res, next) {

    let lastTags = await twitterAPI.lastTags(req.params['tag'], req.params['number']);

    res.json({title: 'fake profiles', count: lastTags.length, profiles: lastTags})
});


router.get('/suspected-profiles', async function (req, res, next) {

    let profileArray = await twitterAPI.getProfile('gantzbe');

    profileArray.map(async pro => {
        let score = bot_test.percentage_of_profile_empty(pro);
        let isRecent = bot_test.creation_date_recent(pro);
        let imageUnique = await bot_test.existing_profile_picture('https://pbs.twimg.com/profile_images/1089931259076329472/W0Xm0sK-_400x400.jpg')
        console.log(score);
    })

    res.json({stam: true});

});

/* GET negative tweets */
router.get('/get-negative-tags/:tag/:number', async function (req, res, next) {

    //let lastTags = await twitterAPI.lastTags(req.params['tag'],req.params['number']);
    // let response = await DeepAI_API.allTweetsNegCheck([{text: "vered"}, {text: "shim"},{text: "may"}, {text: "CONGRATULATIONS to @EricTrump and @LaraLeaTrump on the great news. So proud!"}, {text: "i hate you"}])

    let lastTags = await twitterAPI.getLastTags(req.params['term']);

    // let ans = await DeepAI_API.singleTweetNegCheck({text: "I think what the Democrats are doing with the Border is TREASONOUS. Their Open Border mindset is putting our Country at risk. Will not let this happen. also fuck you"})

    res.json({title: 'last tweets', tweets: lastTweets})
});


module.exports = router;

router.get('/get-last-user-tag/:term', async function (req, res, next) {
    let last_tags = await twitterAPI.getTextTags(req.params['term']);
    res.json({title: 'last tags', tweets: last_tags});

});

module.exports = router;
