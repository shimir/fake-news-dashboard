export function connectionOptions(body, headersToAdd, type) {

    let headers = new Headers()


    headers.append("Content-Type", "application/json")


    if (headersToAdd) {
        headersToAdd.forEach((header) => (headers.append([header[0], header[1]])))
    }


    let options = {}

    if (!type || type === 'POST') {
        options = {
            method: 'POST',
            body: JSON.stringify(body)
        }
    } else if (type === 'GET') {
        options = {
            method: 'GET'
        }
    }

    return options

}

export function connect (url, options) {
    return fetch(url, options)
}

export default {
    connect: connect,
}