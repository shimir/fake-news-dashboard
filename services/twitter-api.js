
var Twitter = require('twitter');

var client = new Twitter({
    consumer_key: '8AM66lqFBl6i1mTq4kg1EZ5zp',
    consumer_secret: 'g28Vo3a8lEQkR7YI8LqKebSzC1HDFpfNHTlbfgYFXNHs0tOnpH',
    access_token_key: '1115650597494054914-oDjvZUx91IzfNCJrgujh8w8bYDIhLU',
    access_token_secret: 'IhZQ9d1nWgWV3MsYV47Vze2aklNDTq6Z6rkRtT7cMxWxP'
});

var params = {screen_name: 'nodejs'};

async function getProfile(screenName) {
    return new Promise((resolve, reject) => {
        client.get('users/lookup', {screen_name: screenName}, function (error, tweets, response) {
            if (error) throw error;
            resolve(tweets);
        });
    });
}

async function searchUsers(searchTerm) {
    return new Promise((resolve, reject) => {
        client.get('users/search', {q: searchTerm}, function (error, tweets, response) {
            if (error) throw error;
            resolve(tweets);
        });
    });
}

function getUserFriends(screen_name) {
    return new Promise((resolve, reject) => {
        client.get('friends/list', {screen_name: screen_name, count: 200}, function (error, friends) {
            if (error) throw error;
            resolve(friends);
        });
    });
}

function getUserTweets(screen_name) {
    return new Promise((resolve, reject) => {
        client.get('statuses/user_timeline', {screen_name: screen_name}, function (error, tweets) {
            if (error) throw error;
            resolve(tweets);
        });
    });
}

// On our account
function getLastTags(screen_name, count = 200) {
    return new Promise((resolve, reject) => {
        client.get('statuses/mentions_timeline', {count: count}, function (error, lastTags) {
            if (error) throw error;
            resolve(lastTags);
        });
    });
}

// Insert any text and get the tweets that match it according to the recent and the popular tags
function getTextTags(q) {
    return new Promise((resolve, reject) => {
        client.get('search/tweets', {q: q, count: 100, result_type: 'recent'}, function (error, relevantTweets) {
            if (error) throw error;
            resolve(relevantTweets);
            var sortedArr = sortAccordingtoPopular(relevantTweets.statuses);
            return sortedArr;

        });
    });
}

function sortAccordingtoPopular(arr) {
    var i = 0;
    while (i < arr.length) {
        arr[i].sum = (arr[i].favorite_count + (2 * arr[i].retweet_count));
        i++;
    }
    arr.sort(function (a, b) {
        return b.sum - a.sum;
    });
    return arr;
}


module.exports.getProfile = getProfile;
module.exports.searchUsers = searchUsers;
module.exports.getFriendsList = getUserFriends;
module.exports.getUserTweets = getUserTweets;
module.exports.getLastTags = getLastTags;
module.exports.getTextTags = getTextTags;
module.exports.sortAccordingtoPopular = sortAccordingtoPopular;