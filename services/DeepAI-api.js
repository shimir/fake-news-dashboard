const deepai = require('deepai'); // OR include deepai.min.js as a script tag in your HTML

deepai.setApiKey('e2e0320f-b57a-4750-a227-ee0cda589f94');


async function checkSingleNegative (tweet) {
    var resp = await deepai.callStandardApi("sentiment-analysis", {
        text: tweet.text
    });
    let isNegative = resp.output.find(function (item) {
        return item == "Negative";
    })

    if (isNegative) {
        return {
            tweet: tweet,
            isNegative: true
        }

    } else {
        return {
            tweet: tweet,
            isNegative: false
        }
    }
}

async function CheckAllNegative(tweets){
    let negative_tweets = [];
    for (let i = 0; i < tweets.length; i++) {
        let result = await checkSingleNegative(tweets[i]);
        if (result.isNegative) {
            negative_tweets.push(result)
        }
    }
    return negative_tweets
}

module.exports.singleTweetNegCheck = checkSingleNegative;
module.exports.allTweetsNegCheck= CheckAllNegative;