let moment = require('moment');
let axios = require('axios');
// let imageSearch = require('node-google-image-search');
const imageSearch = require('image-search-google');

const client = new imageSearch('002178967019750711862:o3deqqee4gi', 'AIzaSyCqnLU5LEN72hxncXG9F8-QKvOHlYWzd70');
const options = {page: 1};

var items = [
    {name: 'existing_profile_picture', properties: {sites: ['google', 'facebook']}, score: 0},
    {name: 'screen_name_fullname_mismatch', properties: {}, score: 0},
    {name: 'activity_location_versus_preferred_language', properties: {}, score: 0},
    {name: 'activity_hours_not_human', properties: {}, score: 0},
    {name: 'creation_date_recent', properties: {}, score: 0},
    {name: 'percentage_of_profile_empty', properties: {}, score: 0,},
    {name: 'cyabra_score', properties: {}, score: 0},
    {name: 'not_native_high_activity', properties: {}, score: 0},
    {name: 'percentage_of_friends_are_active', properties: {}, score: 0},
    {name: 'does_have_2001_following', properties: {}, score: 0},
    {name: 'no_replies_or_retweets', properties: {}, score: 0},
    {name: 'unrealistic_amount_of_tweets', properties: {}, score: 0}, // more than 1000 per month is a bot
];


module.exports = {
    percentage_of_profile_empty: function (profile) {
        let score = 100;

        if (profile.default_profile_image) {
            score = score - 30;
        }

        if (profile.name.indexOf(' ') === -1) {
            score = score - 10
        }

        if (profile.description.length === 0) {
            score = score - 10
        }

        if (!profile.url || profile.url.length === 0) {
            score = score - 5
        }

        if (profile.location.length === 0) {
            score = score - 5;
        }

        return score;
    },
    creation_date_recent: function (profile) {
        let creationDate = profile.created_at;

        let profileCreationDate = moment(creationDate);
        let thisDate = new moment(new Date());

        let monthsDiff = thisDate.diff(profileCreationDate, "months");

        return monthsDiff > 2;
    },
    existing_profile_picture: async function isImageUnique(image_url) {
        return new Promise(async (resolve, reject) => {
            let result = await axios.post('http://localhost:5000/search', JSON.stringify({image_url: image_url}), {
                headers: {'Content-Type': 'application/json'}
            });
            resolve(result.data.links > 3);
        })
    },
    stringSimilarityIndex(profile) {
        // Remove non-english characters
        str1 = profile.name.replace(/[^\x00-\x7F]/g, "").toLowerCase();
        str2 = profile.screen_name.replace(/[^\x00-\x7F]/g, "").toLowerCase();
        if (!str1 || !str2) {
            return false;
        }
        var stringSimilarity = require('string-similarity');
        var similarity = stringSimilarity.compareTwoStrings(str1, str2);
        // Add to score is one is substring od\f the other
        if (str1.search(str2) !== -1 || str2.search(str1) !== -1) {
            similarity += 0.1;
        }
        return similarity > 0.5;
    },

    activity_hours_not_human: function (profile, tweets) {
        var time= new Array(24);
        for(let i = 0; i < time.length; i++) {
            time[i]= 0;
        }
        let tweetTimes = tweets.map((tweet) => { return new Date(tweet.created_at).getHours()});

        for(let i = 0; i < tweetTimes.length; i++) {
            time[tweetTimes[i]]++; // increasing the value at the tweet's hour by one.
        }

        let sum8 = 0;
        let sum16 = 0;

        var i;
        for(i = 0; i < time.length; i++) {
            let k = i;
            var j;
            for(j = i+8; j < i+8+16; j++ ) {
                if(k < i+8) {
                    sum8+= time[k];
                    k++;
                }
                sum16+= time[j % 24];
            } // inside for
            if (sum8 < 0.1 * sum16) {
                return false;
            }
            sum8= 0;
            sum16= 0;
        }
        return true;
    }


};



// check number of unfollow


